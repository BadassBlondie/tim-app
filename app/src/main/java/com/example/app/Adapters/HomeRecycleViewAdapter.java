package com.example.app.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.app.R;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

public class HomeRecycleViewAdapter extends RecyclerView.Adapter<HomeRecycleViewAdapter.ViewHolder>{

    private Context ctx;
    private OnGalleryClickListener onGalleryClickListener;

    private ArrayList<Bitmap> mImageList;
    private List<String> mTextList;
    private List<String> mTagList;
    private List<String> mUploadDateList;
    private List<String> mLatitudeList;
    private List<String> mLongitudeList;

    public HomeRecycleViewAdapter(Context ctx
            , OnGalleryClickListener onGalleryClickClickListener
            , ArrayList<Bitmap> mImageList
            , List<String> mTextList
            , List<String> mTagList
            , List<String> mUploadDateList
            , List<String> mLatitudeList
            , List<String> mLongitudeList) {
        this.ctx = ctx;
        this.onGalleryClickListener = onGalleryClickClickListener;
        this.mImageList = mImageList;
        this.mTextList = mTextList;
        this.mTagList = mTagList;
        this.mUploadDateList = mUploadDateList;
        this.mLatitudeList = mLatitudeList;
        this.mLongitudeList = mLongitudeList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.home_custom_recycle_layout, viewGroup, false);
        ViewHolder holder = new ViewHolder(view, onGalleryClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        Glide.with(ctx)
                .asBitmap()
                .load(mImageList.get(position))
                .into(viewHolder.imageView);

        viewHolder.postTextView.setText(mTextList.get(position));
        viewHolder.postTagView.setText(mTagList.get(position));
        viewHolder.postDateView.setText(mUploadDateList.get(position));

    }

    @Override
    public int getItemCount() {
        return mImageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        OnGalleryClickListener onGalleryClickListener;

        ImageView imageView;
        TextView postTextView;
        TextView postTagView;
        TextView postDateView;
        RelativeLayout relativeLayout;

        public ViewHolder(View itemView, OnGalleryClickListener onGalleryClickListener){
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.cardImage);
            postTextView = (TextView) itemView.findViewById(R.id.postText);
            postTagView = (TextView) itemView.findViewById(R.id.postTag);
            postDateView = (TextView) itemView.findViewById(R.id.postDate);
            relativeLayout = itemView.findViewById(R.id.cardLayout);

            this.onGalleryClickListener = onGalleryClickListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onGalleryClickListener.onGalleryItemClick(getAdapterPosition());
        }
    }

    public interface OnGalleryClickListener{
        void onGalleryItemClick(int position);
    }
}
