package com.example.app.Profile;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.app.ClientDataSource;
import com.example.app.R;

import org.json.JSONObject;
import org.w3c.dom.Text;

public class FragmentProfile extends Fragment{

    private String mToken;

    private ProgressBar progressBar;
    private TextView textViewLogout;
    private TextView textViewName;
    private TextView textViewFullName;
    private TextView textViewFollowers;
    private TextView textViewFollowing;
    private ImageView imageView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            mToken = getArguments().getString("access_token");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_profile, container,false);

        progressBar = (ProgressBar) view.findViewById(R.id.loadingProfile);
        textViewLogout = (TextView) view.findViewById(R.id.textLogout);
        textViewName = (TextView) view.findViewById(R.id.textName);
        textViewFullName = (TextView) view.findViewById(R.id.textFullname);
        textViewFollowers = (TextView) view.findViewById(R.id.textFollowersValue);
        textViewFollowing = (TextView) view.findViewById(R.id.textFollowingValue);
        imageView = (ImageView) view.findViewById(R.id.imgProfile);

        textViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = getActivity();
                activity.finish();
            }
        });

        try {
            ClientDataSource clientDataSource = new ClientDataSource(mToken);
            AsyncTask<Void, Void, String> execute = new ExecuteNetworkOperation(clientDataSource, this.getContext());
            execute.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
        return view;
    }

    public static Fragment newInstance(String mToken) {
        FragmentProfile fragmentProfile = new FragmentProfile();
        Bundle bundle = new Bundle();
        bundle.putString("access_token", mToken);
        fragmentProfile.setArguments(bundle);
        return fragmentProfile;
    }

    public class ExecuteNetworkOperation extends AsyncTask<Void, Void, String> {

        private ClientDataSource clientDataSource;
        private String res;
        private String resFollowers;
        private String resFollowing;
        private Context ctx;

        String name;
        String firstname;
        String lastname;
        Bitmap myBitmap;

        public ExecuteNetworkOperation(ClientDataSource clientDataSource, Context ctx) {
            this.clientDataSource = clientDataSource;
            this.ctx = ctx;
        }

        @Override
        protected String doInBackground(Void... voids) {
            res = clientDataSource.getUserDetails();
            resFollowers = clientDataSource.getUserFollowers();
            resFollowing = clientDataSource.getUserFollowing();
            myBitmap = clientDataSource.getProfileImage();

            try {
                JSONObject jsonObj = new JSONObject(res);
                name = jsonObj.getString("userName");
                firstname = jsonObj.getString("userFirstName");
                lastname = jsonObj.getString("userLastName");

            } catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            textViewName.setText(name);
            textViewFullName.setText(firstname +" "+ lastname);
            textViewFollowers.setText(resFollowers);
            textViewFollowing.setText(resFollowing);
            imageView.setImageBitmap(myBitmap);
        }
    }

}
