package com.example.app.Home;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.app.ClientDataSource;
import com.example.app.R;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

public class FragHomePost extends Fragment implements LocationListener {

    protected LocationManager locationManager;
    protected double latitude;
    protected double longitude;

    private String mToken;
    private ProgressBar progressBar;
    private ImageView imageView;
    private ImageView imageViewSetLocation;
    private Button addButton;
    private Button cancelButton;
    private Button searchImage;
    private Spinner userSpinner;
    private EditText editText;

    private String mChosenUserFullName = "";
    private String mPostText = "";

    private ArrayList<String> userList = new ArrayList<>();
    int SELECT_PICTURE = 200;
    int RESULT_OK = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mToken = getArguments().getString("access_token");
        }

        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getContext()
                , Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext()
                , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity()
                    , new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}
                    , 1);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.frag_home_post, container, false);

        imageView = (ImageView) view.findViewById(R.id.previewImage);
        imageView.setDrawingCacheEnabled(true);
        imageView.buildDrawingCache();

        progressBar = (ProgressBar) view.findViewById(R.id.loading);
        imageViewSetLocation = (ImageView) view.findViewById(R.id.locationAdd);
        addButton = (Button) view.findViewById(R.id.addPost);
        cancelButton = (Button) view.findViewById(R.id.cancel);
        searchImage = (Button) view.findViewById(R.id.selectImage);
        userSpinner = (Spinner) view.findViewById(R.id.spinner_user);
        editText = (EditText) view.findViewById(R.id.setPostDesc);

        imageViewSetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (locationManager != null) {
                    if (ActivityCompat.checkSelfPermission(getContext()
                            , Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext()
                            , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity()
                                , new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}
                                , 1);
                        return;
                    }
                    Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        Toast.makeText(getActivity(),"Latitude:" + latitude + ", Longitude:" + longitude, Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getActivity(),"Could not get location details", Toast.LENGTH_LONG).show();
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = FragmentHome.newInstance(mToken, 2);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frag_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        searchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageChooser();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userList.size() > 0){
                    mChosenUserFullName = userList.get(userSpinner.getSelectedItemPosition());
                }

                if(editText.getText().toString() == null){
                    Toast.makeText(getActivity(),"Please input post description",Toast.LENGTH_LONG).show();
                }
                else{
                    mPostText = editText.getText().toString();

                    AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                            .setTitle("Confirm")
                            .setMessage(mPostText +
                                    "\n\nDo you want to publish post?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        ClientDataSource clientDataSource = new ClientDataSource(mToken);
                                        AsyncTask<Void, Void, String> execute = new AddExecuteNetworkOperation(clientDataSource, getContext());
                                        execute.execute();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getContext(),"Canceled", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .show();
                }
            }
        });

        try {
            ClientDataSource clientDataSource = new ClientDataSource(mToken);
            AsyncTask<Void, Void, String> execute = new ExecuteNetworkOperation(clientDataSource, this.getContext());
            execute.execute();
        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }

    public static FragHomePost newInstance(String mToken){
        FragHomePost fragmentTasks = new FragHomePost();
        Bundle bundle = new Bundle();
        bundle.putString("access_token", mToken);
        fragmentTasks.setArguments(bundle);
        return fragmentTasks;
    }

    public class ExecuteNetworkOperation extends AsyncTask<Void, Void, String> {

        private ClientDataSource clientDataSource;
        private String response;
        private Context ctx;

        ExecuteNetworkOperation(ClientDataSource clientDataSource, Context ctx) {
            this.clientDataSource = clientDataSource;
            this.ctx = ctx;
        }

        @Override
        protected String doInBackground(Void... voids) {
            response = clientDataSource.getFollowedUsers();

            try {
                JSONArray jsonArr = new JSONArray(response);

                for (int i = 0; i < jsonArr.length(); i++) {
                    JSONObject jsonObj = jsonArr.getJSONObject(i);
                    String name = jsonObj.getString("userName");
                    userList.add(name);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);

            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, userList);
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            userSpinner.setAdapter(spinnerAdapter);
        }
    }

    public class AddExecuteNetworkOperation extends AsyncTask<Void, Void, String> {

        private ClientDataSource clientDataSource;
        private Context ctx;

        AddExecuteNetworkOperation(ClientDataSource clientDataSource, Context ctx) {
            this.clientDataSource = clientDataSource;
            this.ctx = ctx;
        }

        @Override
        protected String doInBackground(Void... voids) {
            Bitmap image = Bitmap.createBitmap(imageView.getDrawingCache());
            clientDataSource.newPost(Double.toString(latitude), Double.toString(longitude), mChosenUserFullName, mPostText, image);
            return null;
        }

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(getContext(),"Post published", Toast.LENGTH_SHORT).show();
            Fragment fragment = FragmentHome.newInstance(mToken, 2);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frag_container, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    void imageChooser(){
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_PICTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    imageView.setImageURI(selectedImageUri);
                }
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLatitude();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Provider","disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Provider","enable");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Provider","status");
    }
}
