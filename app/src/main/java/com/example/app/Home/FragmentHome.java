package com.example.app.Home;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.app.Adapters.HomeRecycleViewAdapter;
import com.example.app.ClientDataSource;
import com.example.app.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FragmentHome extends Fragment implements HomeRecycleViewAdapter.OnGalleryClickListener{

    private String mToken;
    private static int start;

    private ProgressBar progressBar;
    private FloatingActionButton floatingActionButton;
    RecyclerView recyclerView;

    private List<String> postIdList = new ArrayList<>();
    private List<String> postUploadDateList = new ArrayList<>();
    private List<String> postTextList = new ArrayList<>();
    private List<String> postTagList = new ArrayList<>();
    private List<String> postLatitudeList = new ArrayList<>();
    private List<String> postLongitudeList = new ArrayList<>();
    private ArrayList<Bitmap> imageList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            mToken = getArguments().getString("access_token");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_home, container,false);

        progressBar = (ProgressBar) view.findViewById(R.id.loadingMyGallery);
        recyclerView = (RecyclerView) view.findViewById(R.id.rec_rec);
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.newPost);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = FragHomePost.newInstance(mToken);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frag_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        try {
            ClientDataSource clientDataSource = new ClientDataSource(mToken);
            AsyncTask<Void, Void, String> execute = new ExecuteNetworkOperation(clientDataSource, this.getContext(), this);
            execute.execute();
        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }

    public static Fragment newInstance(String mToken, int position) {
        FragmentHome fragmentHome = new FragmentHome();
        Bundle bundle = new Bundle();
        bundle.putString("access_token", mToken);
        fragmentHome.setArguments(bundle);
        start = position;
        return fragmentHome;
    }

    @Override
    public void onGalleryItemClick(int position) {
    }

    public class ExecuteNetworkOperation extends AsyncTask<Void, Void, String> {

        private ClientDataSource clientDataSource;
        private HomeRecycleViewAdapter.OnGalleryClickListener onGalleryClickListener;
        private String res;
        private Context ctx;

        public ExecuteNetworkOperation(ClientDataSource clientDataSource, Context ctx, HomeRecycleViewAdapter.OnGalleryClickListener onGalleryClickListener) {
            this.clientDataSource = clientDataSource;
            this.onGalleryClickListener = onGalleryClickListener;
            this.ctx = ctx;
        }

        @Override
        protected String doInBackground(Void... voids) {
            res = clientDataSource.getHomePosts();

            try {
                JSONArray jsonArr = new JSONArray(res);
                JSONArray sortedJsonArray = new JSONArray();

                List<JSONObject> jsonValues = new ArrayList<JSONObject>();
                for (int i = 0; i < jsonArr.length(); i++) {
                    jsonValues.add(jsonArr.getJSONObject(i));
                }

                Collections.sort(jsonValues, new Comparator<JSONObject>() {
                    private static final String KEY_NAME = "postsId";
                    @Override
                    public int compare(JSONObject a, JSONObject b) {
                        Integer str1 =  new Integer("100");
                        Integer str2 = new Integer("100");
                        try {
                            str1 = (Integer) a.get(KEY_NAME);
                            str2 = (Integer) b.get(KEY_NAME);
                        } catch(JSONException e) {
                            Log.e("HOME","Could not sort array");
                        }

                        return -str1.compareTo(str2);
                    }
                });

                for(int i = 0; i < jsonArr.length(); i++) {
                    sortedJsonArray.put(jsonValues.get(i));
                }

                for (int i = 0; i < sortedJsonArray.length(); i++) {
                    JSONObject jsonObj = sortedJsonArray.getJSONObject(i);
                    String postId = jsonObj.getString("postsId");
                    String postUploadDate = jsonObj.getString("postUploadDate");
                    String postLatitude = jsonObj.getString("postLatitude");
                    String postLongitude = jsonObj.getString("postLongitude");
                    String postTag = jsonObj.getString("postTag");
                    String postText = jsonObj.getString("postText");

                    if(postTag.equals("null")){
                        postTag = "";
                    }

                    Bitmap myBitmap = clientDataSource.getImageForPost(postId);

                    imageList.add(myBitmap);
                    postIdList.add(postId);
                    postUploadDateList.add(postUploadDate);
                    postLatitudeList.add(postLatitude);
                    postLongitudeList.add(postLongitude);
                    postTagList.add(postTag);
                    postTextList.add(postText);
                }
            } catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.INVISIBLE);
            HomeRecycleViewAdapter adapter = new HomeRecycleViewAdapter(this.ctx, this.onGalleryClickListener, imageList, postTextList, postTagList, postUploadDateList, postLatitudeList, postLongitudeList);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this.ctx));
        }
    }
}
