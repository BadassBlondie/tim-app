package com.example.app;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.app.Home.FragmentHome;
import com.example.app.Profile.FragmentProfile;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainAuthActivity extends AppCompatActivity {

    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_auth);

        BottomNavigationView bottomNav = findViewById(R.id.bottom_nav);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        token = getIntent().getExtras().getString("access_token");

        getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, FragmentHome.newInstance(token, 0)).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFrag = null;

                    switch (menuItem.getItemId()){
                        case R.id.nav_home:
                            selectedFrag = FragmentHome.newInstance(token, 0);
                            break;
                        case R.id.nav_profile:
                            selectedFrag = FragmentProfile.newInstance(token);
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, selectedFrag).commit();
                    return true;
                }
            };
}
