package com.example.app;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.ContentBody;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class ClientDataSource {

    private String token;
    private String baseUrl = "http://192.168.0.207:43210/private";

    public ClientDataSource(String token){
        this.token=token;
    }

    public String getUserDetails(){ return getResponse("/user/det"); }

    public String getFollowedUsers(){ return getResponse("/user/fr");}

    public String getHomePosts(){ return getResponse("/post/f");}

    public String getUserFollowing(){ return getResponse("/user/fr/nr");}

    public String getUserFollowers(){ return  getResponse("/user/fr/f");}

    public Bitmap getImageForPost(String postId) { return  getImageResponse("/img/postId/" + postId);}

    public Bitmap getProfileImage(){ return getImageResponse("/user/img/p"); }

    public String getResponse(String path){
        URL url = null;
        HttpURLConnection connection = null;

        try {
            url = new URL(baseUrl + path +"?access_token=" +token);

            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Basic dHJ1c3RlZC1jbGllbnQ6c2VjcmV0");

            connection.connect();

            InputStream in = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuffer buffer = new StringBuffer();
            String line = "";

            while ((line = reader.readLine()) != null) {
                buffer.append(line+"\n");
            }

            return buffer.toString();
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            if(connection != null) {
                connection.disconnect();
            }
        }

        return null;
    }

    public Bitmap getImageResponse(String path){
        HttpURLConnection connection = null;
        try {
            URL url = new URL(baseUrl + path + "?access_token=" +token);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setRequestProperty("Accept", "image/jpeg");
            connection.setRequestProperty("Authorization", "Basic dHJ1c3RlZC1jbGllbnQ6c2VjcmV0");
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    public void newPost(String lat, String lot, String tag, String text, Bitmap picture){
        String addUrl ="/post/new";

        HashMap<String, String> params = new HashMap<>();

        params.put("access_token", token);
        params.put("lat", lat);
        params.put("lot", lot);
        params.put("tag", tag);
        params.put("text", text);

        StringBuilder sbParams = new StringBuilder();
        sbParams.append("?");

        try {
            sbParams.append("access_token").append("=")
                    .append(URLEncoder.encode(params.get("access_token"), "UTF-8"));
            sbParams.append("&");
            sbParams.append("lat").append("=")
                    .append(URLEncoder.encode(params.get("lat"), "UTF-8"));
            sbParams.append("&");
            sbParams.append("lot").append("=")
                    .append(URLEncoder.encode(params.get("lot"), "UTF-8"));
            sbParams.append("&");
            sbParams.append("tag").append("=")
                    .append(URLEncoder.encode(params.get("tag"), "UTF-8"));
            sbParams.append("&");
            sbParams.append("text").append("=")
                    .append(URLEncoder.encode(params.get("text"), "UTF-8"));

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String paramsString = sbParams.toString();

        ByteArrayOutputStream blob = new ByteArrayOutputStream();
        picture.compress(Bitmap.CompressFormat.JPEG, 100 , blob);
        byte[] bytes= blob.toByteArray();
        ContentBody bin = new ByteArrayBody(bytes, "file" + getCurrentDate() + ".jpg");

        MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        reqEntity.addPart("file", bin);

        multipost(baseUrl + addUrl + paramsString, reqEntity);
    }

    private static String multipost(String urlString, MultipartEntity reqEntity) {

        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.addRequestProperty("Content-length", reqEntity.getContentLength()+"");
            conn.addRequestProperty(reqEntity.getContentType().getName(), reqEntity.getContentType().getValue());

            OutputStream os = conn.getOutputStream();
            reqEntity.writeTo(conn.getOutputStream());
            os.close();
            conn.connect();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return readStream(conn.getInputStream());
            }

        } catch (Exception e) {
            Log.e("Activity", "multipart post error " + e + "(" + urlString + ")");
        }
        return null;
    }

    private static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

    public String getCurrentDate() {
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(now);
    }
}
